/*
********************** Baza danych dla hurtowni. Automatyczne generowanie zam�wie�
********************** Pawe� Dudzi�ski
********************** Uniwersytet Przyrodniczo-Humanistyczny
********************** Inforamtyka zaoczna 2014/2015
********************** I rok
*/
#include "stdafx.h"//<-linijka Visual Studio
#include <stdio.h>
#include <fstream>
#include <iostream>
//#include <conio.h>
//#include <string>
//#include <cstring>
#include <cstdlib> //rand(); atoi string na int
#include <ctime> // srand
#include <sstream> // ostringstream istringstream <-konwertowanie z int na char i odwrotnie
#include <windows.h>// Sleep

//#include"boost\algorithm\string\predicate.hpp" //dlaczego....

using namespace std;//deklaracja przestrzeni nazw std

//Deklarowanie funkcji
void wypisz_plik();
void menu1();
void stan_menu();
void stan_0();
void stan_1(int x);
void stan_2();
void generuj_zamowienie();
void aktualizacja_danych();

int main()//Funkcja G�owna
{
	//int n = 0;
	menu1();
	return 0;
}

void wypisz_plik()
{
	fstream plik; //uchwyt do pliku

	string tmp = "plik.txt";
	do
	{
		plik.open(tmp);//plik.open("plik.txt"); //otwieramy plik: plik.txt (plik - nazwa pliku, txt - rozszerzenie)
		if (plik.good())
		{
			break;
		}
		else{
			cout << "\nOtwarcie pliku nie powiodlo sie\nSprawdz czy plik jest w poprawnej lokalizacji,\na nastepnie wcisnij ENTER";
			fflush(stdin);//czyszczenie bufora
			getchar();
		}

	} while (true);

	string linia;
	int stan = 0;
	int n = 0;
	string zam = "1";


	cout << "Rodzaj";
	cout.width(13);
	cout << "Producent";
	cout.width(17);
	cout << "Model";
	cout.width(26);
	cout << "Ilosc";
	cout.width(15);
	cout << "\tStan Zamowienia\n\n";

	do
	{

		getline(plik, linia); //pobierz linijke

		if (stan > 4)//nowa linia dla produktu
		{
			stan = 0; //zerowanie zm pomocniczej
			n++;//zliczanie pozycji 
			cout << "\n\n";
		}
		if (n == 10)
		{
			cout << "\nWy�wietl kolejne 10 [ENTER]\n";
			fflush(stdin);//czyszczenie bufora
			getchar();
			n = 0;
		}

		if (stan == 0)
		{
			cout.width(10);
			cout << left << linia;//wypisywanie rodzaju sprzetu
		}
		if (stan == 1)
		{
			cout.width(21);
			cout << left << linia;//wypisywanie producenta sprzetu
		}
		if (stan == 2)
		{
			cout.width(28);
			cout << left << linia;//wypisywanie modelu sprzetu
		}
		if (stan == 3)
		{
			cout.width(7);
			cout << left << linia;//wypisywanie ilosci sprzetu
		}
		if ((stan == 4) && (linia == zam))
		{
			cout.width(12);
			cout << left << "Zamowiono";//wypisywanie ilosci sprzetu
		}
		stan++;


	} while (!plik.eof()); //przerwij je�eli linia bedzie pusta (dane w pliku si� skoncza)

	plik.close(); //zamykamy plik
	//cout << n << endl;
	//cout << stan << endl;
	fflush(stdin);//czyszczenie bufora
	cout << "\n\nWypisywanie zakonczone [Nacisnij ENTER]";
	getchar();
	menu1();
}

void menu1()
{
	system("cls");
	fflush(stdin);
	cout << "Witam w programie: 'Baza danych dla hurtowni. Automatyczne generowanie zam�wie�'\n\n\n";
	char wybor;
	int x = 0;
	int n = NULL;
	cout << "\n1.Wypisz dane z pliku";
	cout << "\n2.Stan";
	cout << "\n3.Generuj zamowienie";
	cout << "\n4.Aktualizacja danych";
	cout << "\nNacisnij Q aby wyjsc\n";
	cout << "\n\nWybieram: ";

	do
	{
		wybor = getchar();
		if (wybor == 49 || wybor == 50 || wybor == 51 || wybor == 52 || wybor == 81 || wybor == 113)

		{
			break;
		}
		else
		{
			cout << "\nBLEDNY WYBOR. Spr ponownie:";
			fflush(stdin);//czyszczenie bufora
		}
	} while (true);

	switch (wybor)
	{
	case 49: wypisz_plik();
		break;
	case 50: stan_menu();
		break;
	case 51: generuj_zamowienie();
		break;
	case 52: aktualizacja_danych();
		break;
	case 27: system("pause");
		break;
	case 118: system("pause");
		break;
	}
}

void stan_menu()
{
	fflush(stdin);//czyszczenie bufora
	system("cls");
	char wybor;
	int x = 0;

	cout << "\n1.Stan 0";
	cout << "\n2.Wypisz produkty ze stanem ponizej podanej wartosci";
	cout << "\n3.Wypisz produkty ze stanem powyzej podanej wartosci";
	//cout << "\n4.Stan konkretnego przedmiotu";
	cout << "\nNacisnij Q aby wrocic\n";
	cout << "\n\nWybieram: ";

	do
	{
		wybor = getchar();
		if (wybor == 49 || wybor == 50 || wybor == 51 || wybor == 52 || wybor == 81 || wybor == 113)

		{
			break;
		}
		else
		{
			cout << "\nBLEDNY WYBOR. Spr ponownie:";
			fflush(stdin);//czyszczenie bufora
		}
	} while (true);

	system("cls");

	switch (wybor)
	{
	case 49: stan_0();
		break;
	case 50: stan_1(x);
		break;
	case 51: x++; stan_1(x);
		break;
	case 52: stan_2();
		break;
	case 81: menu1();
		break;
	case 113: menu1();
		break;
	}
}

void stan_0()
{
	fstream plik;	 //uchwyt do pliku

	do	//petla ktora spr czy plik intnieje
	{
		plik.open("plik.txt");	//otwieranie pliku
		if (plik.good())	//jesli plik otwarto poprawnie to :
		{
			break;	//wyjdz z petli while
		}
		else{	//w przeciwnym wypadku ponow probe otwarcia pliku
			cout << "\nOtwarcie pliku nie powiodlo sie\nSprawdz czy plik jest w poprawnej lokalizacji,\na nastepnie wcisnij ENTER";
			fflush(stdin);	//czyszczenie bufora
			getchar();	//pobieranie znaku
		}

	} while (true);

	string typ, producent, model, ilosc, zamow;	//deklarowanie zmiennych typu string
	int i = 0;	//deklarowanie zminnej pomocniczej do spr czy znaleziono jakies produkty ze stanem == 0
	string spr = "0";
	string zamow_spr = "1";
	while (!plik.eof()) //petla przegladajaca plik po liniach
	{

		getline(plik, typ);	//pobieranie lini zawierajacej typ przedmiotu
		if (typ == "") break;//podczas dopisywania do pliku danych na ko�cu jego pojawia si� "" pusta warto�� co wskazuje na koniec pliku
		getline(plik, producent);	//pobieranie lini zawierajacej model przedmiotu
		getline(plik, model);	//pobieranie lini zawierajacej stan przedmiotu
		getline(plik, ilosc);
		getline(plik, zamow);

		if (ilosc.compare(spr) == 0) //petala spr czy zmienna 'ilosc' typu string przechowuje ciag '0'
		{
			if (i == 0) //jesli znaleziono pierwszy przedmiot to zanim go wypiszemy przygotuje tabelke pod nazwe, typ i ilosc. 
				//Jesli znajdzie 2,3,4,5... itd produkt  "if" zostanie pomi�nty
			{
				cout << "Rodzaj";
				cout.width(13);
				cout << "Producent";
				cout.width(17);
				cout << "Model";
				cout.width(26);
				cout << "Ilosc";
				cout.width(15);
				cout << "\tStan Zamowienia\n\n";

			}

			cout.width(10);
			cout << left << typ;//wypisywanie rodzaju sprzetu

			cout.width(21);
			cout << left << producent;//wypisywanie producenta sprzetu

			cout.width(28);
			cout << left << model;//wypisywanie modelu sprzetu

			cout.width(7);
			cout << left << ilosc;//wypisywanie ilosci sprzetu

			if (zamow_spr == zamow)
			{
				cout.width(12);
				cout << left << "Zamowiono";//wypisywanie ilosci sprzetu
			}

			cout << "\n\n";
			i++; //inklementacja zmiennej potrzebna w dalszej czesci
		}
	}

	if (i == 0) //jesli zmienna pozostanie nie zmieniona jest to r�wnowa�ne z tym ze nie odnaleziono rzadnego produktu ze stanem ==0
		cout << "Obecnie nie brakuje zadnego produktu :D";

	cout << "Nacisnij ENTER aby wrocic";
	fflush(stdin);//czyszczenie bufora
	getchar();
	plik.close(); //zamykamy plik
	stan_menu();
}

void stan_1(int x)
{
	fstream plik;	 //uchwyt do pliku

	do	//petla ktora spr czy plik intnieje
	{
		plik.open("plik.txt");	//otwieranie pliku
		if (plik.good())	//jesli plik otwarto poprawnie to :
		{
			break;	//wyjdz z petli while
		}
		else{	//w przeciwnym wypadku ponow probe otwarcia pliku
			cout << "\nOtwarcie pliku nie powiodlo sie\nSprawdz czy plik jest w poprawnej lokalizacji,\na nastepnie wcisnij ENTER";
			fflush(stdin);	//czyszczenie bufora
			getchar();	//pobieranie znaku
		}

	} while (true);

	string typ, model, ilosc, producent, zamow;
	string zamow_spr = "1";
	int war, p;	//deklarowanie zmiennych typu string

	int i = 0;	//deklarowanie zminnej pomocniczej do spr czy znaleziono jakies produkty ze stanem == 0

	cout << "\n Podaj wartosc: ";
	cin >> war;
	cout << "\n";

	while (!plik.eof()) //petla przegladajaca plik po liniach
	{

		getline(plik, typ);	//pobieranie lini zawierajacej typ przedmiotu
		if (typ == "") break;//podczas dopisywania do pliku danych na ko�cu jego pojawia si� "" pusta warto�� co wskazuje na koniec pliku
		getline(plik, producent);	//pobieranie lini zawierajacej model przedmiotu
		getline(plik, model);	//pobieranie lini zawierajacej stan przedmiotu
		getline(plik, ilosc);
		getline(plik, zamow);

		p = atoi(ilosc.c_str());

		if (x == 0)
		{
			if (p < war)
			{
				if (i == 0) //jesli znaleziono pierwszy przedmiot to zanim go wypiszemy przygotuje tabelke pod nazwe, typ i ilosc. 
					//Jesli znajdzie 2,3,4,5... itd produkt  "if" zostanie pomi�nty
				{
					cout << "\t\tLista artykulow ze stanem mniejszym niz " << war << "\n\n";
					cout << "Rodzaj";
					cout.width(13);
					cout << "Producent";
					cout.width(17);
					cout << "Model";
					cout.width(28);
					cout << "Ilosc";
					cout.width(15);
					cout << "\tStan Zamowienia\n\n";
				}
				cout.width(10);
				cout << left << typ;//wypisywanie rodzaju sprzetu

				cout.width(21);
				cout << left << producent;//wypisywanie producenta sprzetu

				cout.width(28);
				cout << left << model;//wypisywanie modelu sprzetu

				cout.width(7);
				cout << left << ilosc;//wypisywanie ilosci sprzetu

				if (zamow_spr == zamow)
				{
					cout.width(12);
					cout << left << "Zamowiono";//wypisywanie ilosci sprzetu
				}

				cout << "\n\n";
				i++; //inklementacja zmiennej potrzebna w dalszej czesci
			}
		}
		else
		{
			if (p > war)
			{
				if (i == 0) //jesli znaleziono pierwszy przedmiot to zanim go wypiszemy przygotuje tabelke pod nazwe, typ i ilosc. 
					//Jesli znajdzie 2,3,4,5... itd produkt  "if" zostanie pomi�nty
				{
					cout << "\t\tLista artykulow ze stanem wiekszym niz " << war << "\n\n";
					cout << "Rodzaj";
					cout.width(13);
					cout << "Producent";
					cout.width(12);
					cout << "Model";
					cout.width(22);
					cout << "Ilosc";
					cout.width(15);
					cout << "\tStan Zamowienia\n\n";
				}
				cout.width(10);
				cout << left << typ;//wypisywanie rodzaju sprzetu

				cout.width(16);
				cout << left << producent;//wypisywanie producenta sprzetu

				cout.width(22);
				cout << left << model;//wypisywanie modelu sprzetu

				cout.width(8);
				cout << left << ilosc;//wypisywanie ilosci sprzetu

				if (zamow_spr == zamow)
				{
					cout.width(7);
					cout << left << "Zamowiono";//wypisywanie ilosci sprzetu
				}

				cout << "\n\n";
				i++; //inklementacja zmiennej potrzebna w dalszej czesci
			}
		}
	}

	if (i == 0) //jesli zmienna pozostanie nie zmieniona jest to r�wnowa�ne z tym ze nie odnaleziono rzadnego produktu ze stanem ==0
		cout << "Brak Produktow :D";


	cout << "\nNacisnij ENTER aby wrocic";
	fflush(stdin);//czyszczenie bufora
	getchar();
	plik.close();//zamykamy plik
	stan_menu();//zamykamy plik
}

void stan_2()
{

	fstream plik;	 //uchwyt do pliku
	string zamow_spr = "1";

	do	//petla ktora spr czy plik intnieje
	{
		plik.open("plik.txt");	//otwieranie pliku
		if (plik.good())	//jesli plik otwarto poprawnie to :
		{
			break;	//wyjdz z petli while
		}
		else{	//w przeciwnym wypadku ponow probe otwarcia pliku
			cout << "\nOtwarcie pliku nie powiodlo sie\nSprawdz czy plik jest w poprawnej lokalizacji,\na nastepnie wcisnij ENTER";
			fflush(stdin);	//czyszczenie bufora
			getchar();	//pobieranie znaku
		}

	} while (true);

	string szukaj = "Intel";
	string typ, model, ilosc, producent, zamow;
	int i = 0;
	do //petla przegladajaca plik po liniach
	{

		getline(plik, typ);	//pobieranie lini zawierajacej typ przedmiotu
		if (typ == "") break;//podczas dopisywania do pliku danych na ko�cu jego pojawia si� "" pusta warto�� co wskazuje na koniec pliku
		getline(plik, producent);
		getline(plik, model);	//pobieranie lini zawierajacej model przedmiotu
		getline(plik, ilosc);	//pobieranie lini zawierajacej stan przedmiotu
		getline(plik, zamow);

		if (producent == szukaj)
		{
			if (i == 0) //jesli znaleziono pierwszy przedmiot to zanim go wypiszemy przygotuje tabelke pod nazwe, typ i ilosc. 
				//Jesli znajdzie 2,3,4,5... itd produkt  "if" zostanie pomi�nty
			{
				cout << "Rodzaj";
				cout.width(13);
				cout << "Producent";
				cout.width(12);
				cout << "Model";
				cout.width(22);
				cout << "Ilosc";
				cout.width(15);
				cout << "\tStan Zamowienia\n\n";
			}

			cout.width(10);
			cout << left << typ;//wypisywanie rodzaju sprzetu

			cout.width(16);
			cout << left << producent;//wypisywanie producenta sprzetu

			cout.width(22);
			cout << left << model;//wypisywanie modelu sprzetu

			cout.width(8);
			cout << left << ilosc;//wypisywanie ilosci sprzetu

			if (zamow_spr == zamow)
			{
				cout.width(7);
				cout << left << "Zamowiono";//wypisywanie ilosci sprzetu
			}

			cout << "\n\n";
			i++; //inklementacja zmiennej potrzebna w dalszej czesci
		}
	} while (!plik.eof());

	if (i == 0) //jesli zmienna pozostanie nie zmieniona jest to r�wnowa�ne z tym ze nie odnaleziono rzadnego produktu ze stanem ==0
		cout << "Brak Produktow :D";

	cout << "Nacisnij ENTER aby wrocic";
	fflush(stdin);//czyszczenie bufora
	getchar();
	plik.close(); //zamykamy plik
	stan_menu();
}

void generuj_zamowienie()
{
	srand(static_cast<unsigned int>(time(NULL)));

	fstream plik;	 //uchwyt do pliku
	ofstream zrobZamowienie;

	do	//petla ktora spr czy plik intnieje
	{
		plik.open("plik.txt");	//otwieranie pliku
		if (plik.good())	//jesli plik otwarto poprawnie to :
		{
			break;	//wyjdz z petli while
		}
		else{	//w przeciwnym wypadku ponow probe otwarcia pliku
			cout << "\nOtwarcie pliku nie powiodlo sie\nSprawdz czy plik jest w poprawnej lokalizacji,\na nastepnie wcisnij ENTER";
			fflush(stdin);	//czyszczenie bufora
			getchar();	//pobieranie znaku
		}

	} while (true);

	string typ, model, ilosc, producent, zamow;
	int i = 0;
	string ile;
	string nr_zamowienia;
	string zamow_spr = "1";
	char odp;
	int x = 0;
	int ile_towaru_auto;
	string rozszerzenie = ".txt";

	do //petla przegladajaca plik po liniach
	{

		getline(plik, typ);	//pobieranie lini zawierajacej typ przedmiotu
		if (typ == "") break;//podczas dopisywania do pliku danych na ko�cu jego pojawia si� "" pusta warto�� co wskazuje na koniec pliku
		getline(plik, producent);
		getline(plik, model);	//pobieranie lini zawierajacej model przedmiotu
		getline(plik, ilosc);	//pobieranie lini zawierajacej stan przedmiotu
		getline(plik, zamow);

		if (ilosc == "0" && zamow == "0")
		{
			if (i == 0) //jesli znaleziono pierwszy przedmiot to zanim go wypiszemy przygotuje tabelke pod nazwe, typ i ilosc. 
				//Jesli znajdzie 2,3,4,5... itd produkt  "if" zostanie pomi�nty
			{

				i = 1;

				fstream zamowienia;
				string nr;

			step_back:
				do	//petla ktora spr czy plik intnieje
				{
					zamowienia.open("zamowienia.txt", ios::in | ios::out);	//otwieranie pliku
					if (zamowienia.good())	//jesli plik otwarto poprawnie to :
					{
						break;	//wyjdz z petli while
					}
					else{	//w przeciwnym wypadku ponow probe otwarcia pliku
						cout << "\nOtwarcie pliku 'zamowienia.txt' nie powiodlo sie\nSprawdz czy plik jest w poprawnej lokalizacji,\na nastepnie wcisnij ENTER";
						fflush(stdin);	//czyszczenie bufora
						getchar();	//pobieranie znaku
					}

				} while (true);

				int tt = (rand() % 100000000) + 10000000;
				ostringstream ss;
				ss << tt;
				nr_zamowienia = ss.str();

				while (!zamowienia.eof())//je�li plik jest pusty wyjdzie z p�tli
				{
					getline(zamowienia, nr);

					if (nr == nr_zamowienia)
					{
						zamowienia.close();
						goto step_back;
					}

				};

				zamowienia.close();
				zamowienia.open("zamowienia.txt", ios::app);
				zamowienia << nr_zamowienia << "\n";
				zamowienia << "0\n0\n";
				zamowienia.close();

				string otworz = nr_zamowienia + rozszerzenie;
				zrobZamowienie.open(otworz);

				system("cls");
				cout << "Znaleziono produkty.\n\nCzy chcesz wygenerowa� zam�wienie automatycznie dla \nwszytkich przedmiotow czy recznie? \n\n1.Automatycznie\n2.Recznie\n\nWybieram: ";
				fflush(stdin);//czyszczenie bufora
				do
				{
					odp = getchar();
					if (odp == 49 || odp == 50)

					{
						break;
					}
					else
					{
						cout << "\nBLEDNY WYBOR. Spr ponownie:";
						fflush(stdin);//czyszczenie bufora
					}
				} while (true);

				switch (odp)
				{
				case 49:
				{
					cout << "Jaka ilosc ma byc przypisana w zamowieniu dla wszystkich wyszukanych przedmiotow?\nPodaj wartosc: ";
					fflush(stdin);//czyszczenie bufora
					cin >> ile_towaru_auto;
					break;
				}
				case 50:
				{
					x = 1;
					break;
				}
				}
			}
			if (x == 1)
			{
				cout << "Czy chcesz dodac do zamowienia : '" << producent << " " << model << "' ?\n\nT/N\n\n";
				if (getchar() == 84 || getchar() == 116)
				{
					cout << "\nIle sztuk produktu: '" << producent << " " << model << "' chcesz zamowic?\nPodaj wartosc: ";
					fflush(stdin);//czyszczenie bufora
					cin >> ile;
					zrobZamowienie << typ << "\n";
					zrobZamowienie << producent << "\n";
					zrobZamowienie << model << "\n";
					zrobZamowienie << ile << "\n";
				}
			}
			else
			{
				zrobZamowienie << typ << "\n";
				zrobZamowienie << producent << "\n";
				zrobZamowienie << model << "\n";
				zrobZamowienie << ile_towaru_auto << "\n";
			}
		}
	} while (!plik.eof());

	if (i != 0)
	{
		zrobZamowienie.close();
		plik.close();

		cout << "\nWygenerowano zamowienie o nr " << nr_zamowienia << ".\n\nWcisnij ENTER aby wrocic do poprzedniego menu.";
		fflush(stdin);//czyszczenie bufora
		getchar();
		menu1();

	}
	else
	{
		system("cls");
		cout << "Nie odnaleziono brakujacych produktow";


		plik.close();
		fflush(stdin);//czyszczenie bufora
		getchar();
		menu1();
	}
}

void aktualizacja_danych()
{
	fstream zamowienia;
	fstream plik;
	fstream tmp;
	fstream tmp2;
	fstream spr;

	int pusty_plik = 0;
	string czy_zaktualizowano_tmp = "0";
	string plik_zam = "zamowienia.txt";
	string nr_zamowienia, czy_zrealizowano, czy_zaktualizowano, tmp1, pusto;
	string typ, producent, model, ile, czy_zamowiono;
	string typ2, producent2, model2, ile2;
	string plik_nazwa;
	string rozszerzenie = ".txt";

	int stan = 0;
	//spr_ponownie:
	do	//petla ktora spr czy plik intnieje
	{
		plik.open("plik.txt", ios::in);	//otwieranie pliku
		if (plik.good())	//jesli plik otwarto poprawnie to :
		{
			break;	//wyjdz z petli while
		}
		else{	//w przeciwnym wypadku ponow probe otwarcia pliku
			cout << "\nOtwarcie pliku 'plik.txt' albo 'tmp.txt' nie powiodlo sie\nSprawdz czy plik jest w poprawnej lokalizacji,\na nastepnie wcisnij ENTER";
			fflush(stdin);	//czyszczenie bufora
			getchar();	//pobieranie znaku
		}

	} while (true);

	tmp.open("tmp.txt", ios::trunc | ios::out);

	do//p�tla przepisuj�ca zawarto�� bazy do pliku tmp w celu p�niejszego por�wnywania zam�wienia z baz�
	{
		getline(plik, tmp1);
		if (tmp1 == "") break;
		tmp << tmp1 << "\n";
	} while (!plik.eof());

	plik.close();//zamykanie pliku
	tmp.close();//zamykanie pliku
	Sleep(1000);//bajer :]

	plik.open("plik.txt", ios::trunc | ios::out);//wyszed�em z za�o�enia skoro raz ju� uda�o si� otworzy� plik, nie powinno by� problemu z ponownym otwarciem

	tmp.open("tmp.txt", ios::in);//to samo co wcze�niej



	do	//petla ktora spr czy plik intnieje
	{
		zamowienia.open(plik_zam, ios::in);	//otwieranie pliku
		tmp2.open("tmp2.txt", ios::trunc | ios::out);
		if (zamowienia.good() && tmp2.good())	//jesli plik otwarto poprawnie to :
		{
			break;	//wyjdz z petli while
		}
		else
		{	//w przeciwnym wypadku ponow probe otwarcia pliku
			cout << "\nOtwarcie plikuu 'zamowienia.txt' nie powiodlo sie\nSprawdz czy plik jest w poprawnej lokalizacji,\na nastepnie wcisnij ENTER";
			fflush(stdin);	//czyszczenie bufora
			getchar();	//pobieranie znaku
		}

	} while (true);

	while (!zamowienia.eof())//p�tla przepisuj�ca zawarto�� bazy do pliku tmp w celu p�niejszej aktualizacji pliku z nr zam�wie� o nowe statusy (czy zaktualizowano, czy zrealizowano zam�wienie)
		//dodatkowo plik jest otwierany po przez while , bo zak�adam �e plik z numerami zam�wie� mo�e by� pusty
	{
		getline(zamowienia, tmp1);
		if (tmp1 == "") break;//trzeba doda� prewencje przed pustym plikiem dodatkowo, aby pomin�� dalsze wykonywanie kodu
		tmp2 << tmp1 << "\n";
	};

	tmp2.close();//zamykanie pliku
	zamowienia.close();//zamykanie pliku

	tmp2.open("tmp2.txt", ios::in);//wyszed�em z za�o�enia skoro raz ju� uda�o si� otworzy� plik, nie powinno by� problemu z ponownym otwarciem
	zamowienia.open(plik_zam, ios::trunc | ios::out);//to samo co wcze�niej (czyszczenie pliku + mo�liwo�� wprowadzania danych)

	while (!tmp2.eof())
	{
		getline(tmp2, nr_zamowienia);
		if (nr_zamowienia == "") break;//podczas dopisywania do pliku danych na ko�cu jego pojawia si� "" pusta warto�� co wskazuje na koniec pliku
		getline(tmp2, czy_zrealizowano);
		getline(tmp2, czy_zaktualizowano);

		if (czy_zaktualizowano == "0")//sprawdzamy czy zam�wienie zosta�o zsynchronizowane z baz� o status "Zam�wiono"
		{
			plik_nazwa = nr_zamowienia + rozszerzenie;//tworzenie zmiennej z nr. zam�wienia i rozrze�eniem .txt "nr zam.txt" poniewa� tak s� zapisywane szczeg�y zam�wienia (model, ilo��) na dysku

			do
			{
				getline(tmp, typ);//pobieranie lini zawierajacej typ przedmiotu
				if (typ == "") break;//podczas dopisywania do pliku danych na ko�cu jego pojawia si� "" pusta warto�� co wskazuje na koniec pliku
				getline(tmp, producent);
				getline(tmp, model);
				getline(tmp, ile);
				getline(tmp, czy_zamowiono);

				if (ile == "0" && czy_zamowiono == "0")
				{
					spr.open(plik_nazwa, ios::in | ios::out);	//otwieranie pliku
					do
					{
						getline(spr, typ2);
						getline(spr, producent2);
						getline(spr, model2);
						getline(spr, ile2);

						if (model == model2)
						{
							czy_zamowiono = "1";
							spr.close();
							break;
						}

					} while (!spr.eof());
					spr.close();
				}

				plik << typ << "\n";
				plik << producent << "\n";
				plik << model << "\n";
				plik << ile << "\n";
				plik << czy_zamowiono << "\n";
			} while (!tmp.eof());

			plik.close();
			tmp.close();

			czy_zaktualizowano_tmp = "1";
		}

		if (czy_zaktualizowano == "1" && czy_zrealizowano == "1")
		{
			plik_nazwa = nr_zamowienia + rozszerzenie;

			do
			{
				getline(tmp, typ);	//pobieranie lini zawierajacej typ przedmiotu
				if (typ == "") break;//podczas dopisywania do pliku danych na ko�cu jego pojawia si� "" pusta warto�� co wskazuje na koniec pliku
				getline(tmp, producent);
				getline(tmp, model);
				getline(tmp, ile);
				getline(tmp, czy_zamowiono);

				if (czy_zamowiono == "1")
				{
					spr.open(plik_nazwa, ios::in);	//otwieranie pliku
					do
					{
						getline(spr, typ2);
						getline(spr, producent2);
						getline(spr, model2);
						getline(spr, ile2);

						/////////////konwersja stringa na int w celu zsumowania ilo�ci na stanie z ilo�ci� zam�wienia
						if (model == model2)
						{
							int v;
							istringstream iss1(ile);
							iss1 >> v;

							int b;
							istringstream iss2(ile2);
							iss2 >> b;

							v += b;//sumowanie stanu z zam�wieniem

							ostringstream ss1;
							ss1 << v;

							ile = ss1.str();
							czy_zamowiono == "0";

							spr.close();
							break;
						}
						//////////////////////////////////
					} while (!spr.eof());
					spr.close();
				}

				plik << typ << "\n";
				plik << producent << "\n";
				plik << model << "\n";
				plik << ile << "\n";
				plik << czy_zamowiono << "\n";
			} while (!tmp.eof());

			plik.close();
			tmp.close();
		}

		zamowienia << nr_zamowienia << "\n";
		zamowienia << czy_zrealizowano << "\n";
		zamowienia << czy_zaktualizowano_tmp << "\n";
	}

	tmp2.close();//zamykanie pliku
	zamowienia.close();//zamykanie pliku

	if (stan == 0)
	{
		stan++;
		//goto spr_ponownie;
	}

	cout << "Zakonczono aktualizacje daych.\n\nNacisnij ENTER aby powrocic do poprzedniego menu";
	fflush(stdin);//czyszczenie bufora
	getchar();
	menu1();
}